import React, { Component } from 'react'
import kids from './kids.jpg'
import drunk from './drunk.jpg'
import fixed from './fixed.jpg'
import './App.css'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title"
            style={{
              fontSize: 50,
            }}
          >
            HAPPY BIRTHDAY!!
          </h1>
          <div
            style={{
              height: '500px',
              display: 'flex',
              flexDirection: 'row',
            }}
          >
            <div style={{margin: '10px'}}>
              <img
                src={kids}
                alt="kids"
                className="spin-left"
                style={{
                	maxWidth: '100%',
				        	maxHeight: '100%',
                }}
              />
            </div>
            <div style={{margin: '10px'}}>
              <img
                src={fixed}
                alt="fixed"
                style={{
                	maxWidth: '100%',
				        	maxHeight: '100%',
                }}
              />
            </div>
            <div style={{margin: '10px'}}>
              <img
                src={drunk}
                alt="drunk"
                className="spin-right"
                style={{
                	maxWidth: '100%',
				        	maxHeight: '100%',
                }}
              />
            </div>
          </div>
        </header>
        <h1 className="App-title"
          style={{
            fontSize: 50,
          }}
        >
          EMPOWER SPORTS
        </h1>
        <h1 className="App-title"
          style={{
            fontSize: 50,
          }}
        >
          <a href="http://💪⚽.ws">
            <span role="img" aria-label="empower sports.ws">
              💪⚽.ws
            </span>
          </a>
        </h1>
      </div>
    )
  }
}

export default App
